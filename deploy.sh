#!/bin/bash
# Pull the latest changes from GitLab
git pull origin master

# Activate the virtual environment
source restapi-django/env/Lib/Scripts/activate

# Install/update dependencies
pip install -r requirements.txt

# Collect static files
python manage.py collectstatic --noinput

# Migrate the database 
python manage.py migrate

# Stop the PythonAnywhere web app
bash /var/www/manmohansingh_pythonanywhere_com_wsgi.py stop

# Start the PythonAnywhere web app
bash /var/www/manmohansingh_pythonanywhere_com_wsgi.py start
